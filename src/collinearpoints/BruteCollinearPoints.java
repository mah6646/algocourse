package collinearpoints;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class BruteCollinearPoints {

    private final List<LineSegment> segments = new ArrayList<>();

    public BruteCollinearPoints(Point[] points) {
        if (points == null)
            throw new IllegalArgumentException("input is null");

        for (int i = 0; i < points.length; i++) {
            if (points[i] == null)
                throw new IllegalArgumentException("A point element is Null");
        }
        Point[] copy = Arrays.copyOf(points, points.length);

        Arrays.sort(copy); // so we can find the duplicate elements / unique check below
        // validations
        for (int i = 0; i < copy.length; i++) {
            if (i > 0 && copy[i].compareTo(copy[i - 1]) == 0) {
                throw new IllegalArgumentException("At least two Points are same. Pass unique points");
            }
        }

        collectSegments(copy);
    } // finds all line segments containing 4 points

    private void collectSegments(Point[] points) {
        // by requirement atmost 4 collinear points are expected.
        for (int i = 0; i <= points.length - 3; i++) {
            for (int j = i + 1; j < points.length - 2; j++) {
                for (int k = j + 1; k < points.length - 1; k++) {
                    for (int m = k + 1; m < points.length; m++) {
                        Point p = points[i];
                        Point q = points[j];
                        Point r = points[k];
                        Point s = points[m];

                        double pq = p.slopeTo(q);
                        double qr = q.slopeTo(r);
                        double rs = r.slopeTo(s);

                        if (pq == qr && qr == rs) {
                            Point[] quads = { p, q, r, s };
                            Arrays.sort(quads);
                            segments.add(new LineSegment(quads[0], quads[quads.length - 1]));
                        }
                    }
                }
            }

        }
    }

    public int numberOfSegments() {
        return segments.size();
    } // the number of line segments

    public LineSegment[] segments() {
        return segments.toArray(new LineSegment[0]);
    } // the line segments
}