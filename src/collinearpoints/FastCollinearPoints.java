package collinearpoints;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class FastCollinearPoints {

    private static final int MIN_POINTS_ON_LINE = 3;
    private final List<LineSegment> segments = new ArrayList<>();
    
    public FastCollinearPoints(Point[] points) {
        if (points == null)
            throw new IllegalArgumentException("input is null");

        for (int i = 0; i < points.length; i++) {
            if (points[i] == null)
                throw new IllegalArgumentException("A point element is Null");
        }
        Point[] copy = Arrays.copyOf(points, points.length);
        
        Arrays.sort(copy); // so we can find the duplicate elements / unique check below
        // validations
        for (int i = 0; i < copy.length; i++) {
            if (i > 0 && copy[i].compareTo(copy[i - 1]) == 0) {
                throw new IllegalArgumentException("At least two Points are same. Pass unique points");
            }
        }

        collectSegments(copy);
    } // finds all line segments containing 4 or more points

    private List<List<Point>> groupBySlopeWithP(Point[] rest) {
        List<List<Point>> lists = new ArrayList<>();
        Point p = rest[0];
        Point prev = rest[0];
        List<Point> list = new ArrayList<>();
        list.add(prev);
        lists.add(list);

        for (int i = 1; i < rest.length; i++) {
            Point q = rest[i];
            double pqSlope = p.slopeTo(q);
            double ppSlope = p.slopeTo(prev);

            if (pqSlope != ppSlope) {
                list = new ArrayList<>();
                lists.add(list);
            }
            list.add(q);
            prev = q;
        }
        return lists;
    }

    private void collectSegments(Point[] points) {

        for (int i = 0; i < points.length; i++) {
            Point p = points[i];

            Comparator<Point> comparator = p.slopeOrder();
            // sort REST of the points by the slot they make with p
            Point[] pointsCopy = Arrays.copyOf(points, points.length);
            Arrays.sort(pointsCopy, comparator);

            List<List<Point>> pointsGroupedBySlope = this.groupBySlopeWithP(pointsCopy); // when sorted wrt P, P will be
                                                                                         // first point in the array
            pointsGroupedBySlope.removeIf(x -> x.size() < MIN_POINTS_ON_LINE);

            for (List<Point> list : pointsGroupedBySlope) {
                list.add(p);
                Point[] pp = list.toArray(new Point[0]);
                Arrays.sort(pp);
                Point a = pp[0];
                if (!a.equals(p))
                    continue;
                Point b = pp[pp.length - 1];
                segments.add(new LineSegment(a, b));
            }
        }
    }

    public int numberOfSegments() {
        return segments.size();
    } // the number of line segments

    public LineSegment[] segments() {
        return segments.toArray(new LineSegment[0]);
    } // the line segments
}